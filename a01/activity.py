num_year = int(input("Enter a year: \n"))

def is_leap(year):
	if (year % 400 == 0) or (year % 4 == 0 and year % 100 != 0):
		return True

	return False

if is_leap(num_year):
	print(f"{num_year} is a leap year")
else:
	print(f"{num_year} is not a leap year")

num1 = int(input("please enter a num: \n"))
num2 = int(input("please enter 2nd num \n"))

for i in range(num1):
	for r in range(num2):
		print("* ",end="")

	print()